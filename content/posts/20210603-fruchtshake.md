---
title: "Fruchtshake"
date: 2021-06-03T13:51:16+02:00
draft: false
tags: [Früchte]
categories: [Rezepte]
---

| Menge | Zutaten |
| --- | --- |
| 1 | Avocado |
| 2 | kleine Bananen |
| 400ml | Hafermilch |

1. Früchte zu Brei mixen
1. Hafermilch hinzugeben und ordentlich vermixen
