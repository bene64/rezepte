---
title: "Schockokuchen"
date: 2021-06-25T13:51:16+02:00
draft: false
tags: [vegan, backen]
categories: [Rezepte]
---

# Zutaten für eine Auflaufform (4 Portionen)

| Menge | Zutat|
| --- | --- |
| 6 | Eier | 
| 1 Prise | Salz | 
| 400 g | Bitterschokolade (mind. 60% Kakaogehalt) | 
| 300 g | weiche Butter | 
| 200 g | Zucker | 
| 1 Msp. | frisch ausgekratztes Vanillemark (alternativ 1 Päckchen Vanillezucker und dafür 1 TL Zucker weniger) | 
| 1 große Prise | Zimt | 
| 3 bis 4 EL | Weizenmehl, Type 405 | 
| 4 EL | Kakaopulver | 

# Anleitung

1. Den Backofen auf 200° Ober-/Unterhitze vorheizen. Die Eier trennen. Die Eiweiße mit dem Salz zu steifem Schnee schlagen. 
1. Die Schokolade in Stücke brechen oder hacken, mit der Butter in eine Metallschüssel geben und über einem heißen Wasserbad schmelzen lassen. 
1. Den Zucker mit den Eigelben schaumig aufschlagen und mit dem Vanillemark bzw. dem Vanillezucker und dem Zimt würzen. Die flüssige Schoko-Butter-Mischung unter die Eigelbmasse rühren. Dann vorsichtig den Eischnee unterziehen. Das Mehl und den Kakao hineinsieben und alles locker vermengen. Den Teig in eine mit Backpapier ausgelegte Springform (26 cm Durchmesser) füllen. Die Form in den vorgeheizten Backofen schieben und den Kuchen ca. 25 Minuten backen. (Er sollte innen noch schön feucht sein und wird beim Abkühlen wieder fest.) Den Kuchen herausnehmen und abkühlen lassen.


Quelle: [ Youtube - Felicitas Then - Der beste Schokoladenkuchen | damit habe ich THE TASTE gewonnen](https://www.youtube.com/watch?v=eZnwVRcjFoU)
